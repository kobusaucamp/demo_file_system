package com.rtjvm.scala.oop.filesystem

import java.util.Scanner

import com.rtjvm.scala.oop.command.Command
import com.rtjvm.scala.oop.files.Directory

/**
  * User: kobusa
  * Date: 2019/01/14
  * Time: 11:14 AM
  */
object Filesystem extends App {


  val root = Directory.ROOT
  var state = State(root, root)
  val scanner = new Scanner(System.in)

  while(true) {

    state.show
    val input = scanner.nextLine()
    state = Command.from(input).apply(state)

  }


}
