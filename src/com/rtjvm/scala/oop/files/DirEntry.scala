package com.rtjvm.scala.oop.files

/**
  * User: kobusa
  * Date: 2019/01/14
  * Time: 11:55 AM
  */
abstract class DirEntry(val parentPath: String, val name: String) {

  def path: String = parentPath + Directory.SEPARATOR + name

  def asDirectory: Directory

  def getType: String = "Directory"

}
