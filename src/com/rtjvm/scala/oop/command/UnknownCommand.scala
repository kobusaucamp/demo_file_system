package com.rtjvm.scala.oop.command
import com.rtjvm.scala.oop.filesystem.State

/**
  * User: kobusa
  * Date: 2019/01/14
  * Time: 12:39 PM
  */
class UnknownCommand extends Command {

  override def apply(state: State): State =
    state.setMessage("Command not found!")

}
